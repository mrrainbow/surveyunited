# Form Maker
 A fullstack online form maker app with authentication system, created with NextJS & TailwindCSS.

<br/>
 <img src="https://i.imgur.com/w8pg1aN.png" alt="App preview"/>
 <br/>

## Key features
- Authentication system (using JWT)
- Create, update and delete custom forms
- Send invitation throught email to submit a form
- Share a link to submit a form
- See form answers with detail displayed in a table

## Enviornment Variables

Read [this](/env-vars.md) for more info on setting up the enviornment variables.

## Information

Regarding demos & vercel deployment


### Live Demo:
https://form-maker.vercel.app 

### Gallery:
https://imgur.com/a/ha7G0fg

### Deploy with Vercel:

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https%3A%2F%2Fgithub.com%2Fvercel%2Fnext.js%2Ftree%2Fcanary%2Fexamples%2Fhello-world&env=ACCESS_TOKEN_SECRET&envDescription=ACCESS_TOKEN_SECRET%20is%20needed%20for%20the%20account%20system%20to%20work.%20Look%20at%20the%20link%20for%20more%20details&envLink=https%3A%2F%2Fgitlab.com%2Fmrrainbow%2Fsurveyunited%2F-%2Fblob%2Fmain%2Fenv-vars.md&demo-title=SuveyUnited&demo-description=The%20survey%20united%20website&demo-url=https%3A%2F%2Fsurveyunited.vercel.app%2F)
