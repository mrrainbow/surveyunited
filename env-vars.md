# Survey United Enviornment Variables

`ACCESS_TOKEN_SECRET` is needed for the account system to work.
`MONGODB_URI` is needed to store data.

## How to setup `ACCESS_TOKEN_SECRET`

### Step 1

Go to https://www.cryptool.org/en/cto/openssl/

### Step 2

Once the terminal loads, paste the following:

`openssl rand -hex 20`

### Step 3

You should see your secret like so:

<img src="/public/static/img/readme/step3.png">
<br>

### Step 4

Copy that secret

### Step 5

There are multiple variations of this step. Look below.


## Step 5 (Vercel)

### Step 5:1:1

Literally just use the button below, it should work.

[![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/clone?repository-url=https%3A%2F%2Fgithub.com%2Fvercel%2Fnext.js%2Ftree%2Fcanary%2Fexamples%2Fhello-world&env=ACCESS_TOKEN_SECRET&envDescription=ACCESS_TOKEN_SECRET%20is%20needed%20for%20the%20account%20system%20to%20work.%20Look%20at%20the%20link%20for%20more%20details&envLink=https%3A%2F%2Fgitlab.com%2Fmrrainbow%2Fsurveyunited%2F-%2Fblob%2Fmain%2Fenv-vars.md&demo-title=SuveyUnited&demo-description=The%20survey%20united%20website&demo-url=https%3A%2F%2Fsurveyunited.vercel.app%2F)


## Step 5 (Static Hosting & Local Hosting)

### Step 5:2:1

Change the .env.example to .env

### Step 5:2:2

Replace only the `ENTER_YOUR_ACCESS_TOKEN_SECRET` part with the generated secret mentioned above.

### Step 5:2:3

Deploy your project, it should work now.

## How to setup `MONGODB_URI`

coming soon!